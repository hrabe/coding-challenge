﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Lake_Barbacue;

namespace Lake_Barbacue
{
    internal class Program
    {
        // [0,0] => [a,b]
        internal static bool IsPointOnLineSegmentFromOrigin(Point point, double a, double b){
                if(a > 0 && (point.X < 0 || point.X > a )) return false;
                if(a < 0 && (point.X > 0 || point.X < a )) return false;
                if(b > 0 && (point.Y < 0 || point.Y > b )) return false;
                if(b < 0 && (point.Y > 0 || point.Y < b )) return false;
                return true;
        }

        internal static void solve(Point[] points, double a, double b, double r){

            int max = 0;
            Point circle = null;

            List<Point> centers = new List<Point>();
 
            if(a == 0 && b == 0){
                centers = new List<Point> { new Point{X = 0, Y = 0}};
            }
            else if (b == 0){
                // centers = points.SelectMany(p => GetIntersectionY(p, r).Where(p => p != null)).ToList();;
                foreach(Point p in points){
                    var inter = Geometry.GetIntersectionY(p, r);
                    if (inter != null)
                    centers.AddRange(inter);
                }
            }
            else{
                // centers = points.SelectMany(p => GetIntersectionX(p, S, P, R).Where(p => p != null)).ToArray();
                foreach(Point p in points){
                    var inter = Geometry.GetIntersectionX(p, a, b, r);
                    if (inter != null)
                    centers.AddRange(inter);
                }
            }

            int count = centers.Count();

            foreach(Point center in centers){
                int currMax = 0;
                // if(!IsPointOnLineSegmentFromOrigin(center, a, b)) continue;
                foreach(Point point in points){
                    if(Geometry.IsInsideCircle(point, center, r)){
                        currMax++;
                    }
                }
                if(max < currMax) {
                    max = currMax;
                    circle = new Point{X=center.X, Y = center.Y};
                }
            }
            Console.WriteLine($"----------- {max} -----------");
            Console.WriteLine($"----------- X: [{circle?.X}, Y: {circle?.Y}] -----------");
            File.WriteAllText("python\\circle.txt", $"{circle.X} {circle.Y} {r}");
        }


        public static void Main(string[] args)
        {
            string currentDir = Directory.GetCurrentDirectory();
            string inputFilePath = Path.Join(currentDir, args[0]);
            
            long N = 0, R = 0, A = 0, B = 0; 

            Point[] boats;
            Point a = new Point{X = 0, Y = 0};
            Point b = new Point();

            using (StreamReader reader = new StreamReader(inputFilePath))
            {
                string line;
                if ((line = reader.ReadLine()) != null) {
                    string[] firstLine = line.Split(' ');
                    N = long.Parse(firstLine[0]);
                    R = long.Parse(firstLine[1]);
                    A = long.Parse(firstLine[2]);
                    B = long.Parse(firstLine[3]);
                    b.X = A;
                    b.Y = B;
                }

                boats = new Point[N];

                int j = 0;
                while ((line = reader.ReadLine()) != null || j < N ) {
                    string[] lineSplit = line.Split(' ');
                    boats[j] = new Point{X = long.Parse(lineSplit[0]), Y = long.Parse(lineSplit[1])};
                    j++;
                }
            }

            // boats inside radius
            Point[] boatsInsideRadius = Geometry.GetPointsInsideRadiusGivenLineAB(boats, a, b, R);

            File.WriteAllText("python\\points.txt", $"{A} {B}{Environment.NewLine}");
            File.AppendAllLines("python\\points.txt", boatsInsideRadius.Select(p => $"{p.X} {p.Y}"));

            solve(boatsInsideRadius, A, B, R);
            
            Console.WriteLine($"A: x={a.X}, y={a.Y}");
            Console.WriteLine($"B: x={b.X}, y={b.Y}");
            Console.WriteLine($"N: {N}");
            Console.WriteLine($"R: {R}");
            Console.WriteLine($"Boats: {boats?.Length}");
            Console.WriteLine($"Boats Inside: {boatsInsideRadius?.Length}");   
        }
    }
}
