Lake Barbecue
=========================================================

## Input

1. The first line contains N (number of boats), R (smoke radius), A (pier's x coordinate) and B (pier's y coordinate).
2. The next N lines contain x and y boat coordinates
x1, y1
x2, y2
...
x<sub>n</sub>, y<sub>n</sub>

## Output

Maximum number of boats such that lie in a circle with R radius and center on line segment [0,0] and [A,B].

## Examples
```
dotnet run --project .\csharp\src\Lake-Barbacue.csproj .\csharp\test\testInputs\202142_Input.in
.\python\setupVenv.ps1
python .\python\showGraph.py
deactivate
```